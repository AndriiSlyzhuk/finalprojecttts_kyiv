import { JobStatusService } from './../services/job-status.service';
import { JobStatus } from './../interfaces/job-status';
import { JobTask } from './../interfaces/job-task';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { JobTaskService } from '../services/job-task.service';
import { UserService } from '../services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {  IncludesInMassiveValidator } from '../helpers/custom-validator';
import { User } from '../interfaces/user';
import { LoggedUser } from '../interfaces/logged-user';
import { JobTaskAccess } from '../helpers/job-task-access';

@Component({
  selector: 'app-job-task-detail',
  templateUrl: './job-task-detail.component.html',
  styleUrls: ['./job-task-detail.component.scss']
})
export class JobTaskDetailComponent implements OnInit {

  id: number;
  task: JobTask;
  errorTask = '';
  taskFormView: FormGroup;
  assignees: User[] = [];
  status: JobStatus[] = [];
  readonlyManager = new JobTaskAccess();
  titleTask = 'Task';
  cancelBack = 'Back';
  editState = false;
  curUser: LoggedUser;


  constructor(private taskServ: JobTaskService, private userServ: UserService, private statusServ: JobStatusService,
              private authServ: AuthService, private actRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.curUser = this.authServ.getCurrentUserValue();
    this.actRoute.params.subscribe(params => {
      console.log(params['id']) ;
      this.id = params['id'];
    });
    this.createForm();
    this.getTasksById(this.id);
    this.getStatus();
    if (this.curUser.role === 'Manager'){
      this.getAssignee();
    }

  }

  createForm(){
    this.taskFormView = new FormGroup({
      title: new FormControl(''),
      description: new FormControl('', Validators.required),
      startedDate: new FormControl(''),
      deadline: new FormControl(''),
      status: new FormControl(''),
      owner: new FormControl(''),
      assignee: new FormControl(''),
      remark: new FormControl(''),
    });

  }

  setValueForm(){
      this.taskFormView.controls.title.setValue(this.task.title);
      this.taskFormView.controls.description.setValue(this.task.description, Validators.required);
      this.taskFormView.controls.startedDate.setValue(String(this.task.startedDate).substring(0, 10));
      this.taskFormView.controls.deadline.setValue(String(this.task.deadline).substring(0, 10));
      this.taskFormView.controls.status.setValue(this.task.status);
      this.taskFormView.controls.owner.setValue(this.task.owner);
      this.taskFormView.controls.assignee.setValue(this.task.assignee, IncludesInMassiveValidator(this.assignees.map(x => x.login)));
      this.taskFormView.controls.remark.setValue(this.task.remark);
  }

  getTasksById(id: any){
    return this.taskServ.getJobTaskId(id).subscribe(
      (data) => {
        console.log(data);
        this.task = data;
        this.setValueForm();
        this.errorTask = '';
      },
      (ex) =>{
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
        console.log(this.errorTask);
      }
    );
  }

  getAssignee(){
    return this.userServ.getAssignees().subscribe(
      (data) =>{
        console.log(data);
        this.assignees = data;
      },
      (ex) =>{
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
      }
    );
  }

  getStatus(){
    return this.statusServ.getJobStatus().subscribe(
      (data) =>{
        console.log(data);
        this.status = data;
        this.errorTask = '';
      },
      (ex) =>{
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
      }
    );
  }

  save(){
    this.readonlyManager = new JobTaskAccess();
    this.editState = false;
    this.titleTask = 'Task (saving)';
    this.taskServ.updateJobTask(this.id, this.taskFormView.value).subscribe(
      () => {
        this.titleTask = 'Task (successfully saved)';
        this.errorTask = '';
      },
      (ex) => {
        console.log(ex);
        this.titleTask = 'Task (save failed)';
        this.editState = true;
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
      }
    );
  }


  back(){
    this.router.navigate(["/job-task"]);
  }

  edit(){
    this.editState = true;
    this.titleTask = 'Task (edit)';
    if (this.curUser.role === 'Manager')
    {
      this.readonlyManager.title = false;
      this.readonlyManager.description = false;
      this.readonlyManager.startedDate = false;
      this.readonlyManager.deadline = false;
      this.readonlyManager.assignee = false;
      this.readonlyManager.status = false;
      this.readonlyManager.remark = false;
    }
    else if(this.curUser.role === 'Specialist'){
      this.readonlyManager.status = false;
      this.readonlyManager.remark = false;
    }
  }

  updateValidAssignees(){
    this.taskFormView.controls.assignee.setValidators(IncludesInMassiveValidator(this.assignees.map(x => x.login)));
  }

  updateValidStatus(){
    this.taskFormView.controls.status.setValidators(IncludesInMassiveValidator(this.status.map(x => x.name)));
  }

}
