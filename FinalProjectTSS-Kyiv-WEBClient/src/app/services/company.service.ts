import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Company } from '../interfaces/company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  host = environment.apiUrl;

constructor(private http: HttpClient) { }

getCurrentCompany(): Observable<Company>{
  return this.http.get<Company>(this.host + 'api/Company/singleCompany');
}

updateCompany(id: any, company: any){
  return this.http.put(this.host + 'api/Company/' + id, company);
}

}
