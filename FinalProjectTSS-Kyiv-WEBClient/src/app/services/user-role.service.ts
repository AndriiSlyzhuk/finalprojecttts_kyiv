import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserRole } from '../interfaces/user-role';

@Injectable({
  providedIn: 'root'
})
export class UserRoleService {

  host = environment.apiUrl;

constructor(private http: HttpClient) { }

getRoles(): Observable<UserRole[]>{
  return this.http.get<UserRole[]>(this.host + 'api/Role');
}

}
