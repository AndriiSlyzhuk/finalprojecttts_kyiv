/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JobStatusService } from './job-status.service';

describe('Service: JobStatus', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobStatusService]
    });
  });

  it('should ...', inject([JobStatusService], (service: JobStatusService) => {
    expect(service).toBeTruthy();
  }));
});
