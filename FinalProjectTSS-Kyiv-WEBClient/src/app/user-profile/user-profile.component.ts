import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IncludesInMassiveValidator } from '../helpers/custom-validator';
import { UserProfileAccess } from '../helpers/user-profile-access';
import { LoggedUser } from '../interfaces/logged-user';
import { UserProfile } from '../interfaces/user-profile';
import { UserRole } from '../interfaces/user-role';
import { AuthService } from '../services/auth.service';
import { UserProfileService } from '../services/user-profile.service';
import { UserRoleService } from '../services/user-role.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  curUser: LoggedUser;
  curId: number;
  curUserProfile: UserProfile;
  errorUserPro = '';
  errorArray = [];

  userProFrom: FormGroup;
  createdForm = false;
  cancelBack = 'Back';
  editState = false;
  titleUserProfile = 'User Profile';
  readonlyAccess = new UserProfileAccess();
  roles: UserRole[] = [];

  constructor(private userProServ: UserProfileService, private authServ: AuthService, private roleServ: UserRoleService, private actRoute: ActivatedRoute) {  }

  ngOnInit() {
    this.curUser = this.authServ.getCurrentUserValue();
    this.actRoute.params.subscribe(params => {
      console.log(params['id']) ;
      this.curId = params['id'];
    });
    this.getProfile();
    this.getRole();
  }

  getProfile(){
    this.userProServ.getUserById(this.curId).subscribe(
      (data) => {
        this.curUserProfile = data;
        this.createForm();
      },
      (ex) => {
        if(ex.status == 0)
        {
          this.errorUserPro = 'Server unavailable';
        }
        else{
          this.errorUserPro = ex.error;
        }
      }
    )
  }

  getRole(){
    return this.roleServ.getRoles().subscribe(
      (data) =>{
        console.log(data);
        this.roles = data;
      },
      (ex) =>{
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorUserPro = 'Server unavailable';
        }
        else{
          this.errorUserPro = ex.error;
        }
      }
    );
  }

  createForm(){
    this.userProFrom = new FormGroup({
      userId: new FormControl(this.curUserProfile.userId),
      login: new FormControl(this.curUserProfile.login),
      role: new FormControl(this.curUserProfile.role),
      firstName: new FormControl(this.curUserProfile.firstName),
      secondName: new FormControl(this.curUserProfile.secondName),
      email: new FormControl(this.curUserProfile.email, Validators.email),
      birthdayDay: new FormControl(this.curUserProfile.birthdayDay.substring(0, 10)),
    });
    this.createdForm = true;
  }

  cancel(){
    this.editState = false;
    this.titleUserProfile = 'User Profile';
  }

  edit(){
    this.editState = true;
    this.titleUserProfile = 'User Profile (edit)';
    this.setAccess();
  }

  setAccess(){
    if (this.curUser.userId === this.curUserProfile.userId )
    {
      this.readonlyAccess.login = false;
      this.readonlyAccess.firstName = false;
      this.readonlyAccess.secondName = false;
      this.readonlyAccess.email = false;
      this.readonlyAccess.birthdayDay = false;
    }else if(this.curUser.role = "Manager"){
      this.readonlyAccess.role = false;
    }
  }

  save(){

    this.readonlyAccess = new UserProfileAccess();
    this.editState = false;
    this.titleUserProfile = 'User Profile (saving)';
    this.userProServ.updateUserById(this.userProFrom.value.userId, this.userProFrom.value).subscribe(
      () => {
        this.titleUserProfile = 'User Profile (successfully saved)';
        this.errorUserPro = "";
      },
      (ex) => {
        console.log(ex);
        this.titleUserProfile = 'User Profile (save failed)';
        this.editState = true;
        this.setAccess();
        if(ex.status == 0)
        {
          this.errorUserPro= 'Server unavailable';
        }else if (ex.status === 400) {
          // handle validation error
          for(let er in ex.error.errors)
          {
            for(let itemErorr of ex.error.errors[er]){
              this.errorArray.push(itemErorr);
            }
          }
        }
        else{
          this.errorUserPro = ex.error;
        }
      }
    );
  }

  updateValidRoles(){
    this.userProFrom.controls.role.setValidators(IncludesInMassiveValidator(this.roles.map(x => x.name)));
  }

}
