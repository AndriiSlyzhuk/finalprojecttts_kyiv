import { JobStatusComponent } from './job-status/job-status.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './helpers/auth-guard';
import { LoginComponent } from './login/login.component';
import { JobTaskComponent } from './job-task/job-task.component';
import { JobTaskDetailComponent } from './job-task-detail/job-task-detail.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CompanyComponent } from './company/company.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'users', component: UsersComponent,  canActivate: [AuthGuard],
                    data: {
                      role: ['Manager']
                    }
  },
  { path: 'list-status', component: JobStatusComponent,  canActivate: [AuthGuard],
                    data: {
                      role: ['Manager']
                    } },
  { path: 'job-task', component: JobTaskComponent },
  { path: 'job-task/:id', component: JobTaskDetailComponent },
  { path: 'user-profile/:id', component: UserProfileComponent },
  { path: 'company', component: CompanyComponent,  canActivate: [AuthGuard],
                    data: {
                      role: ['Manager']
                    }  },
  { path: '**', component: HomeComponent}

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      // onSameUrlNavigation: 'ignore',
      onSameUrlNavigation: "reload",
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
