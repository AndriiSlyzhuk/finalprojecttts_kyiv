import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { CustomErrorStateMatcher } from '../helpers/custom-error-state-matcher';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  reigisterError: string;
  matcher = new CustomErrorStateMatcher();
  password = '';

  constructor(private authServ: AuthService, private router: Router, private formBuilder: FormBuilder) {  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      login: [''],
      password: ['',[ Validators.minLength(6), Validators.required]],
      confirmPass: ['']
    }, { validator: this.checkPasswords });
    this.reigisterError = '';

  }

  register(){
    this.authServ.register(this.registerForm.value).subscribe(
      () => {
        this.reigisterError = '';
        this.router.navigate(['/login']);
      },
      (ex) => {
        this.reigisterError = ex.error;
      }
    )
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPass.value;

    return pass === confirmPass ? null : { notSame: true };
  }


}
