export interface UserProfile {
  userId: number;
  login: string;
  role: string;
  firstName: string;
  secondName: string;
  email: string;
  birthdayDay: string;
}
