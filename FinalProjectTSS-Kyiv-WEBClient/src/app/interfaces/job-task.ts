export interface JobTask {
  id: number;
  title:	string;
  description:	string;
  startedDate: Date;
  deadline: Date;
  remark:	string;
  status:	string;
  owner:	string;
  assignee:	string;
  execution: number;
}

