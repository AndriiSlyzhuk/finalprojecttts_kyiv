﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class UserRegisterModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string confirmPass { get; set; }
    }
}
