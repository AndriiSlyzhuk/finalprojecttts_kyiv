﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class JobTaskModel
    {
        public int id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime StartedDate { get; set; }

        public DateTime Deadline { get; set; }

        public string Remark { get; set; }

        public string Status { get; set; }

        public string Owner { get; set; }

        public string Assignee { get; set; }

        public decimal StatusOrder { get; set; }

        public decimal? Execution { get; set; } 

    }
}
