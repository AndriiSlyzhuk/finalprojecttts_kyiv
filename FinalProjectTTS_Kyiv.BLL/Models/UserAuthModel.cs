﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class UserAuthModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
