﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class JobTaskNewModel
    {
        public JobTaskNewModel()
        {
            this.StartedDate = DateTime.Now;
        }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime StartedDate { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        public string Remark { get; set; }

        public string Status { get; set; }

        public string Owner { get; set; }

        [Required]
        public string Assignee { get; set; }
    }
}
