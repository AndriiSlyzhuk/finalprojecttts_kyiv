﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class JobStatusService : IJobStatusService
    {
        readonly IUnitOfWork uow;
        readonly IJobStatusRepository statusRepo;
        readonly IMapper mapper;

        public JobStatusService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.statusRepo = uow.JobStatusRepository;
            this.mapper = mapper;
        }

        public async Task AddAsync(JobStatusNewModel model)
        {
            var checkName = await statusRepo.GetByAsync(x => x.Name.ToLower() == model.Name.ToLower());
            if (checkName != null) { throw new Exception("Status with same name exist in DB"); }

            var dao = mapper.Map<JobStatusNewModel, JobStatus>(model);
            await statusRepo.CreateAsync(dao);
            await uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id, bool softDelete = true)
        {
            var daoContext = await statusRepo.GetByIdAsync(id);
            if(daoContext is null) { throw new Exception ("Status doesn`t exist in DB"); }

            if (daoContext.Order == 0) { throw new Exception("You can`t delete order this element"); }

            await statusRepo.DeleteAsync(e => e.Id == id);
            await uow.SaveAsync();
        }

        public async Task<IEnumerable<JobStatusModel>> GetAll()
        {   
            return  mapper.Map<IEnumerable<JobStatus>, IEnumerable<JobStatusModel>>(await statusRepo.GetAllAsync());
        }

        public async Task<JobStatusModel> GetByIdAsync(int id)
        {
            return mapper.Map<JobStatus, JobStatusModel>(await statusRepo.GetByIdAsync(id));
        }

        public async Task UpdateAsync(JobStatusModel model)
        {
            var checkName = await statusRepo.GetByAsync(x => x.Name.ToLower() == model.Name.ToLower());
            if (checkName != null) { throw new Exception("Status with same name exist in DB"); }

            var daoContext = await statusRepo.GetByIdAsync(model.Id);
            if (daoContext is null) { throw new Exception("Status doesn`t exist in DB"); }

            if(daoContext.Order == 0) { throw new Exception("You can`t update order this element"); }

            var dao = mapper.Map<JobStatusModel, JobStatus>(model, daoContext);
            await statusRepo.Update(dao);
            await uow.SaveAsync();
        }
    }
}
