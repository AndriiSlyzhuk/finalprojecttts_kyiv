﻿using FinalProjectTTS_Kyiv.BLL.Infrastructure.Helpers;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class AuthService : IAuthService
    {
        readonly IUnitOfWork _uow;
        readonly IRepository<User> _userRepository;
        readonly AppSettings _appSettings;

        public AuthService(IUnitOfWork uow, IOptions<AppSettings> appSettings)
        {
            _uow = uow;
            _userRepository = _uow.UserRepository;
            _appSettings = appSettings.Value;
        }

        public async Task<UserLoggedModel> Authenticate(UserAuthModel userAuthModel)
        {
            var user = await _userRepository.GetByAsync(u => u.Login == userAuthModel.Login && u.Password == userAuthModel.Password);
            if (user == null)
                throw new Exception("Login or password are invalid. Try again");

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.Name) 
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            UserLoggedModel userLoggedDto = new UserLoggedModel { UserId = user.Id, Login = user.Login, Role = user.Role.Name, Token = tokenHandler.WriteToken(token) };

            return userLoggedDto;
        }

        public async Task RegisterAsync(UserRegisterModel userRegister)
        {
            if(userRegister.Password != userRegister.confirmPass)
                throw new Exception("Password doesn't match");

            var user = await _userRepository.GetByAsync(u => u.Login == userRegister.Login);
            if (user != null)
            {
                throw new Exception("User " + user.Login + " exist in database");
            }

            await _userRepository.CreateAsync(new User
            {
                Login = userRegister.Login,
                Password = userRegister.Password,
                RoleId = _uow.RoleRepository.GetByAsync(x => x.Name == "Specialist").Result.Id
            });

            await _uow.SaveAsync();
        }
    }
}
