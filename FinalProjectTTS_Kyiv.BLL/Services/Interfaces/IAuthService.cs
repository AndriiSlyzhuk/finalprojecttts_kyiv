﻿using FinalProjectTTS_Kyiv.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IAuthService
    {
        Task RegisterAsync(UserRegisterModel userRegister);

        Task<UserLoggedModel> Authenticate(UserAuthModel userAuthModel);
    }
}
