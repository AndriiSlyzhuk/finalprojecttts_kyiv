﻿using FinalProjectTTS_Kyiv.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IRoleService: ICrud<RoleModel>
    {
        Task AddAsync(RoleNewModel model);
    }
}
