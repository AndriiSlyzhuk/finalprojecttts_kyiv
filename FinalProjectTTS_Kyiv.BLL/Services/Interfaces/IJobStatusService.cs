﻿using FinalProjectTTS_Kyiv.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IJobStatusService: ICrud<JobStatusModel>
    {
        Task AddAsync(JobStatusNewModel model);
    }
}
