﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class UserProfileService : IUserProfileService
    {

        readonly IUnitOfWork uow;
        readonly IUserProfileRepository userProRepo;
        readonly IUserRepository userRepo;
        readonly IMapper mapper;
        readonly IHttpContextAccessor httpContextAccessor;

        public UserProfileService(IUnitOfWork uow, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            this.uow = uow;
            this.userProRepo = uow.UserProfileRepository;
            this.userRepo = uow.UserRepository;
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task AddAsync(UserProfileModel model)
        {
            var daoUserContext = await userRepo.GetByIdAsync(model.UserId);
            if (daoUserContext is null) { throw new Exception("User doesn`t exist in DB because you can`t update profile "); }
            var daoUser = mapper.Map<UserProfileModel, User>(model, daoUserContext);

            if (daoUser.Role.Name != model.Role)
            {
                var curUser = httpContextAccessor.HttpContext.User;
                if (!curUser.IsInRole("Manager")) { throw new Exception("User with current role can`t change role in profile"); }

                var daoRole = await uow.RoleRepository.GetByAsync(x => x.Name == model.Role);
                if (daoRole is null) { throw new Exception("Role doesn`t exist in DB"); }                

                daoUser.RoleId = daoRole.Id;
            }
            await userRepo.Update(daoUser);

            var daoUserProContext = await userProRepo.GetByIdAsync(daoUserContext.UserProfile?.Id ?? 0);
            var daoUserProfile = mapper.Map<UserProfileModel, UserProfile>(model, daoUserProContext);
            if (daoUserProContext is null)
                await userProRepo.CreateAsync(daoUserProfile);
            else
                await userProRepo.Update(daoUserProfile);

            await uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id, bool softDelete = true)
        {
            var daoUser = await userRepo.GetByIdAsync(id);
            var profileId = daoUser.UserProfile?.Id ?? 0;
            if (profileId != 0)
                await userProRepo.DeleteAsync(x => x.Id == profileId, false);

        }

        public async Task<IEnumerable<UserProfileModel>> GetAll()
        {
            var daos = await userRepo.GetAllAsync();
            return mapper.Map<IEnumerable<User>, IEnumerable<UserProfileModel>>(daos);
        }

        public async Task<UserProfileModel> GetByIdAsync(int id)
        {
            var dao = await userRepo.GetByIdAsync(id);
            return mapper.Map<User, UserProfileModel>(dao);
        }

        public async Task UpdateAsync(UserProfileModel model)
        {
            var daoUserContext = await userRepo.GetByIdAsync(model.UserId);
            if (daoUserContext is null) { throw new Exception("User doesn`t exist in DB because you can`t update profile "); }
            var daoUser = mapper.Map<UserProfileModel, User>(model, daoUserContext);

            if (daoUser.Role.Name != model.Role)
            {
                var curUser = httpContextAccessor.HttpContext.User;
                if (!curUser.IsInRole("Manager")) { throw new Exception("User with current role can`t change role in profile"); }

                var daoRole = await uow.RoleRepository.GetByAsync(x => x.Name == model.Role);
                if (daoRole is null) { throw new Exception("Role doesn`t exist in DB"); }

                daoUser.RoleId = daoRole.Id;
            }
            await userRepo.Update(daoUser);

            var daoUserProContext = await userProRepo.GetByIdAsync(daoUserContext.UserProfile?.Id ?? 0);
            var daoUserProfile = mapper.Map<UserProfileModel, UserProfile>(model, daoUserProContext);
            if (daoUserProContext is null)
                await userProRepo.CreateAsync(daoUserProfile);                
            else
                await userProRepo.Update(daoUserProfile);

                

            await uow.SaveAsync();
        }
    }
}
