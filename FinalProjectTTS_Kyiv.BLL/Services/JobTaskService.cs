﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Infrastructure;
using FinalProjectTTS_Kyiv.BLL.Infrastructure.Filters;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class JobTaskService: IJobTaskService
    {
        //TODO: IDispose
        readonly IUnitOfWork uow;
        readonly IJobTaskRepository taskRepo;
        readonly IMapper mapper;
        readonly IHttpContextAccessor httpContextAccessor;
        readonly IMessageService messageServ;

        public JobTaskService(IUnitOfWork uow, IMapper mapper, IHttpContextAccessor httpContextAccessor, IMessageService messageServ)
        {
            this.uow = uow;
            this.taskRepo = uow.JobTaskRepository;
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
            this.messageServ = messageServ;
        }

        public async Task AddAsync(JobTaskNewModel model)
        {
            //TODO: check from many sessions
            string curUserId = httpContextAccessor.HttpContext.User.Identity.Name;
            var dao = mapper.Map<JobTaskNewModel, JobTask>(model);

            var daoStatus = await uow.JobStatusRepository.GetByAsync(x => x.Order == 0);
            if (daoStatus is null)
                throw new Exception("Default status doesn`t exist in DB!");
            else
                dao.StatusId = daoStatus.Id;

            var userRepo = uow.UserRepository;

            var daoOwner = await userRepo.GetByAsync(x => x.Login == model.Owner);
            if (daoOwner is null) 
            {
                var curUser = await userRepo.GetByAsync(x => x.Id.ToString() == curUserId);
                if (curUser is null)
                    throw new Exception("Owner doesn`t exist!");
                else
                    dao.OwnerId = curUser.Id;
            }                
            else
                dao.OwnerId = daoOwner.Id;

            var daoAssignee = await userRepo.GetByAsync(x => x.Login == model.Assignee);
            if (daoAssignee is null)
                throw new Exception("Assignee doesn`t exist!");
            else
                dao.AssigneeId = daoAssignee.Id;

            if (dao.StartedDate == default(DateTime))
                dao.StartedDate = DateTime.Now;

            if(model.Deadline.Date < model.StartedDate.Date)
                throw new Exception("Deadline date must be greater then start date");

            await taskRepo.CreateAsync(dao);
            await uow.SaveAsync();

            try { await messageServ.SendMessageAsync(dao); }
            catch { throw new Exception("Task is created but message does`t send"); }
            
        }

        public async Task DeleteByIdAsync(int id, bool softDelete = true)
        {
            var daoContext = await taskRepo.GetByIdAsync(id);
            if (daoContext is null) { throw new Exception("Task doesn`t exist in DB"); }
            await taskRepo.DeleteAsync(e => e.Id == id);
            await uow.SaveAsync();
        }

        public async Task<IEnumerable<JobTaskModel>> GetAll()
        {
            return mapper.Map<IEnumerable<JobTask>, IEnumerable<JobTaskModel>>(await taskRepo.GetAllAsync());
        }

        public async Task<ApiResult<JobTaskModel>> GetAllWithFilter(JobTaskFilter filter, PageSetting pageSetting)
        {
            var customFilter = filterForTaskRepo(filter);

            var daos = await taskRepo.GetAllByAsync(customFilter);
            var mapModels = mapper.Map<IEnumerable<JobTask>, IEnumerable<JobTaskModel>>(daos);

            var allStatus = await uow.JobStatusRepository.GetAllAsync();
            var maxOrder = Convert.ToDecimal(allStatus.Max(x => x.Order));
            if (maxOrder != 0)
                mapModels = mapModels.Select(x => { x.Execution = (x.StatusOrder / maxOrder) * 100; return x; }); 

            var apiResultTask = ApiResult<JobTaskModel>.CreatePageSetting(mapModels.AsQueryable(), pageSetting);
            return apiResultTask;
        }

        Expression<Func<JobTask, bool>> filterForTaskRepo(JobTaskFilter filter)
        {
            // x =>
            ParameterExpression pe = Expression.Parameter(typeof(JobTask), "task");
            Expression predicateBody = Expression.Empty();
            var condList = new List<Expression>();

            if (filter.Title != null)
            {
                // x.Title.ToLower() == filter.Title.ToLower()
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("Title"));
                Expression leftCall = Expression.Call(leftProp, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                Expression right = Expression.Constant(filter.Title.ToLower());
                Expression exp = Expression.Equal(leftCall, right);
                condList.Add(exp);
            }

            if (filter.TitleContains != null)
            {
                // x.Title.ToLower().Contains(filter.Title.ToLower())
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("Title"));
                Expression leftCall = Expression.Call(leftProp, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                Expression right = Expression.Constant(filter.TitleContains.ToLower());

                MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });

                var exp = Expression.Call(leftCall, method, right);
                //Expression exp = Expression.Equal(leftCall, right);
                condList.Add(exp);
            }

            if (filter.Status != null)
            {
                // x.Status.Name.ToLower() == filter.Status.ToLower()
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("Status"));
                Expression leftPropValue = Expression.Property(leftProp, typeof(JobStatus).GetProperty("Name"));
                Expression leftCall = Expression.Call(leftPropValue, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                Expression right = Expression.Constant(filter.Status.ToLower());
                Expression exp = Expression.Equal(leftCall, right);
                condList.Add(exp);
            }
            

            if (filter.Assignee != null)
            {
                // x.Assignee.Login.ToLower() == filter.Assignee.ToLower()
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("Assignee"));
                Expression leftPropValue = Expression.Property(leftProp, typeof(User).GetProperty("Login"));
                Expression leftCall = Expression.Call(leftPropValue, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                Expression right = Expression.Constant(filter.Assignee.ToLower());
                Expression exp = Expression.Equal(leftCall, right);
                condList.Add(exp);
            }

            if (filter.Owner != null)
            {
                // x.Owner.Login.ToLower() == filter.Owner.ToLower()
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("Owner"));
                Expression leftPropValue = Expression.Property(leftProp, typeof(User).GetProperty("Login"));
                Expression leftCall = Expression.Call(leftPropValue, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                Expression right = Expression.Constant(filter.Owner.ToLower());
                Expression exp = Expression.Equal(leftCall, right);
                condList.Add(exp);
            }


            if (filter.StartedDateFrom != null)
            {
                // x.StartedDate >= filter.StartedDateFrom
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("StartedDate"));
                Expression right = Expression.Constant(filter.StartedDateFrom);
                Expression exp = Expression.GreaterThanOrEqual(leftProp, right);
                condList.Add(exp);
            }

            if (filter.StartedDateTo != null)
            {
                // x.StartedDate <= filter.StartedDateTo
                Expression leftProp = Expression.Property(pe, typeof(JobTask).GetProperty("StartedDate"));
                Expression right = Expression.Constant(filter.StartedDateTo);
                Expression exp = Expression.LessThanOrEqual(leftProp, right);
                condList.Add(exp);
            }


            if (!condList.Any())
                predicateBody = Expression.Constant(true);

            foreach (var condItem in condList )
            {
                if (condList.First() == condItem)
                {
                    predicateBody = condItem;
                    continue;
                }                    
                predicateBody = Expression.AndAlso(predicateBody, condItem);
            }

            return Expression.Lambda<Func<JobTask, bool>>(predicateBody, new ParameterExpression[] { pe });
        }

        public async Task<JobTaskModel> GetByIdAsync(int id)
        {
            return mapper.Map<JobTask, JobTaskModel>(await taskRepo.GetByIdAsync(id));
        }

        public async Task UpdateAsync(JobTaskModel model)
        {
            var daoContext = await taskRepo.GetByIdAsync(model.id);
            if(daoContext is null) { throw new Exception("Task doesn`t exist in DB"); }
            
            var dao = mapper.Map<JobTaskModel, JobTask>(model, daoContext);

            if (dao.Status.Name != model.Status)
            {
                var daoStatus = await uow.JobStatusRepository.GetByAsync(x => x.Name == model.Status);
                if (daoStatus is null)
                    throw new Exception("Job status doesn`t exist!");
                else
                    dao.StatusId = daoStatus.Id;
            }

            var userRepo = uow.UserRepository;
            if (dao.Owner.Login != model.Owner)
            {
                var daoOwner = await userRepo.GetByAsync(x => x.Login == model.Owner);
                if (daoOwner is null)
                    throw new Exception("Owner doesn`t exist!");
                else
                    dao.OwnerId = daoOwner.Id;
            }
            if (dao.Assignee.Login != model.Assignee)
            {
                var daoAssignee = await userRepo.GetByAsync(x => x.Login == model.Owner);
                if (daoAssignee is null)
                    throw new Exception("Assignee doesn`t exist!");
                else
                    dao.AssigneeId = daoAssignee.Id;
            }

            if (dao.Deadline.Date < dao.StartedDate.Date)
                throw new Exception("Deadline date must be greater then starte date");

            await taskRepo.Update(dao);
            await uow.SaveAsync();

            try { await messageServ.SendMessageAsync(dao); }
            catch { throw new Exception("Task is updated but message does`t send"); }
        }

    }
}
