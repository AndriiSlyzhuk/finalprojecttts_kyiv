﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class CompanyService: ICompanyService
    {
        readonly IUnitOfWork uow;
        readonly ICompanyRepository companyRepo;
        readonly IMapper mapper;

        public CompanyService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.companyRepo = uow.CompanyRepository;
            this.mapper = mapper;
        }

        public async Task<CompanyModel> GetCurrentCompany()
        {
            var daoContext =  await companyRepo.GetAllAsync();
            var dao = daoContext.SingleOrDefault();
            if(dao is null) { throw new Exception("Company doesn`t exist in DB"); }

            return mapper.Map<Company, CompanyModel>(dao);
        }

        public async Task UpdateAsync(CompanyModel model)
        {
            var daoContext = await companyRepo.GetByIdAsync(model.Id);
            if (daoContext is null) { throw new Exception("Company doesn`t exist in DB"); }

            var dao = mapper.Map<CompanyModel, Company>(model, daoContext);
            await companyRepo.Update(dao);
            await uow.SaveAsync();
        }
    }
}
