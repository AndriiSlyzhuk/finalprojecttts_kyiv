﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.Filters
{
    public class JobTaskFilter
    {
        public string Title { get; set; }

        public string TitleContains { get; set; }

        public string Status { get; set; }

        public DateTime? StartedDateFrom { get; set; }

        public DateTime? StartedDateTo { get; set; }

        public DateTime? DeadlineFrom { get; set; }

        public DateTime? DeadlineTo { get; set; }

        public string Owner { get; set; }

        public string Assignee { get; set; }
    }
}
