﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService
{
    public class OptionSMTP
    {
        public string Server { get; set; }

        public int Port { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public bool EnableSsl { get; set; } = true;
    }
}
