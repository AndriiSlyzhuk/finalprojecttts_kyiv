﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService
{
    public class SMTPSender: IDisposable
    {
        readonly SmtpClient client;
        private bool disposedValue;

        public SMTPSender()
        {
            this.client = new SmtpClient();
        }

        public void SendMessage(OptionSMTP option, Message message)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(message.AddressFrom);
            foreach(var address in message.AddressTo)
            {
                try { mailMessage.To.Add(new MailAddress(address)); } catch { }
                
            }
            mailMessage.Subject = message.Subject;
            mailMessage.Body = message.Body;

            client.Host = option.Server;
            client.Port = option.Port;

            client.Credentials = new System.Net.NetworkCredential(option.Login, option.Password) ;
            client.EnableSsl = option.EnableSsl;

            client.Send(mailMessage);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    client.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~SMTPSender()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
