﻿using FinalProjectTTS_Kyiv.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace FinalProjectTTS_Kyiv.DAL.Data
{
    public class TTSAppContext : DbContext
    {

        public TTSAppContext(DbContextOptions<TTSAppContext> options) : base(options)
        {
            Database.EnsureCreated();
        }


        public DbSet<Company> Companies { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<JobTask> JobTasks { get; set; }
        public DbSet<JobStatus> JobStatus { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Company>().HasData(new Company { Id = 1, Name = "Your Company"});

        //    modelBuilder.Entity<User>().HasData(new User { Id = 1, Login = "admin", Password = "1111", RoleId =1 });

        //    modelBuilder.Entity<Role>().HasData(new Role[] { new Role { Id = 1, Name = "Manager" }, new Role { Id = 2, Name = "Specialist" } });

        //    modelBuilder.Entity<JobStatus>().HasData(new JobStatus { Id = 1, Name = "Created", Order = 0 });
        //}

    }
}
