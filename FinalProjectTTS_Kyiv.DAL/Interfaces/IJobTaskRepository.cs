﻿using FinalProjectTTS_Kyiv.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Interfaces
{
    public interface IJobTaskRepository: IRepository<JobTask>
    {
    }
}
