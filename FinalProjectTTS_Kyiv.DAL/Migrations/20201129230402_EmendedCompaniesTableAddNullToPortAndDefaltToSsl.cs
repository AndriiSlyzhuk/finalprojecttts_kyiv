﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalProjectTTS_Kyiv.DAL.Migrations
{
    public partial class EmendedCompaniesTableAddNullToPortAndDefaltToSsl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SmtpPort",
                table: "Companies",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SmtpPort",
                table: "Companies",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
