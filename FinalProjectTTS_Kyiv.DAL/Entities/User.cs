﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    public class User : BaseEntity
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
