﻿using FinalProjectTTS_Kyiv.DAL.Data;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Implementations.Repositories
{
    public class JobStatusRepository : IJobStatusRepository
    {

        readonly TTSAppContext context;
        readonly DbSet<JobStatus> curSet;

        public JobStatusRepository(TTSAppContext context)
        {
            this.context = context;
            this.curSet = context.JobStatus;
        }

        public async Task CreateAsync(JobStatus item)
        {
            await curSet.AddAsync(item);
        }

        public async Task DeleteAsync(Expression<Func<JobStatus, bool>> expression, bool softDelete = true)
        {
            var item = await curSet.FirstOrDefaultAsync(expression);

            if (item != null)
            {
                if (softDelete)
                {
                    item.IsDeleted = true;
                    curSet.Update(item);
                }
                else
                    curSet.Remove(item);
            }
        }

        public async Task<List<JobStatus>> GetAllAsync()
        {
            return await curSet.Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<List<JobStatus>> GetAllByAsync(Expression<Func<JobStatus, bool>> expression)
        {
            return await curSet.Where(x => !x.IsDeleted).Where(expression).ToListAsync();
        }

        public async Task<JobStatus> GetByAsync(Expression<Func<JobStatus, bool>> expression)
        {
            return await curSet.Where(x => !x.IsDeleted).FirstOrDefaultAsync(expression);
        }

        public async Task<JobStatus> GetByIdAsync(int id)
        {
            return await curSet.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(JobStatus item)
        {
            var itemExist = await curSet.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (itemExist != null)
            {
                if (!itemExist.IsDeleted)
                {
                    curSet.Update(item);
                }
            }
        }
    }
}
