﻿using FinalProjectTTS_Kyiv.DAL.Data;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Implementations.Repositories
{
    public class JobTaskRepository : IJobTaskRepository
    {

        readonly TTSAppContext context;
        readonly DbSet<JobTask> curSet;

        public JobTaskRepository(TTSAppContext context)
        {
            this.context = context;
            this.curSet = context.JobTasks;
        }

        public async Task CreateAsync(JobTask item)
        {
            await curSet.AddAsync(item);
        }

        public async Task DeleteAsync(Expression<Func<JobTask, bool>> expression, bool softDelete = true)
        {
            var item = await curSet.FirstOrDefaultAsync(expression);

            if (item != null)
            {
                if (softDelete)
                {
                    item.IsDeleted = true;
                    curSet.Update(item);
                }
                else
                    curSet.Remove(item);
            }
        }

        public async Task<List<JobTask>> GetAllAsync()
        {
            return await curSet.Include(x => x.Status)
                        .Include(x => x.Owner)
                        .Include(x => x.Assignee).Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<List<JobTask>> GetAllByAsync(Expression<Func<JobTask, bool>> expression)
        {
            return await curSet.Include(x => x.Status)
                        .Include(x => x.Owner)
                        .Include(x => x.Assignee).Where(x => !x.IsDeleted).Where(expression).ToListAsync();
        }

        public async Task<JobTask> GetByAsync(Expression<Func<JobTask, bool>> expression)
        {
            return await curSet.Include(x => x.Status)
                        .Include(x => x.Owner)
                        .Include(x => x.Assignee).Where(x => !x.IsDeleted).FirstOrDefaultAsync(expression);
        }

        public async Task<JobTask> GetByIdAsync(int id)
        {
            return await curSet.Include(x => x.Status)
                        .Include(x => x.Owner)
                        .Include(x => x.Assignee).Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(JobTask item)
        {
            var itemExist = await curSet.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (itemExist != null)
            {
                if (!itemExist.IsDeleted)
                {
                    curSet.Update(item);
                }
            }
        }
    }
}
