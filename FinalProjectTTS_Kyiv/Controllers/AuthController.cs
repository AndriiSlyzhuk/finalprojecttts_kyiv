﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        readonly IAuthService authServ;

        public AuthController(IAuthService authServ)
        {
            this.authServ = authServ;
        }

        // GET api/<AuthenticationController>/5
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task Register([FromBody] UserRegisterModel userRegister)
        {
            try
            {
                await authServ.RegisterAsync(userRegister);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        //// POST api/<AuthenticationController>
        [AllowAnonymous]
        [HttpPost]
        public async Task<UserLoggedModel> Login(UserAuthModel userAuthDto)
        {
            try
            {
                return await authServ.Authenticate(userAuthDto);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
