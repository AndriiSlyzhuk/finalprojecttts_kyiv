﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        readonly IUserProfileService userProServ;

        public UserProfileController(IUserProfileService userProServ)
        {
            this.userProServ = userProServ;
        }

        // GET: api/<UserProfileController>
        [Authorize(Roles = "Manager")]
        [HttpGet]
        public async Task<IEnumerable<UserProfileModel>> Get()
        {
            try
            {
                return await userProServ.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<UserProfileController>/5
        [HttpGet("{id}")]
        public async Task<UserProfileModel> Get(int id)
        {

            try
            {
                //TODO: if user change role but using previous token, he will have access
                if (User.Identity.Name != id.ToString() && !User.IsInRole("Manager"))
                    throw new ApplicationException("Access is dinied");
                return await userProServ.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<UserProfileController>
        [HttpPost]
        public async Task Post([FromBody] UserProfileModel model)
        {
            try
            {
                if (User.Identity.Name != model.UserId.ToString() && !User.IsInRole("Manager"))
                    throw new ApplicationException("Access is dinied");
                await userProServ.AddAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<UserProfileController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] UserProfileModel model)
        {
            try
            {
                if (User.Identity.Name != id.ToString() && !User.IsInRole("Manager"))
                    throw new ApplicationException("Access is dinied");
                model.UserId = id;
                await userProServ.UpdateAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<UserProfileController>/5
        [Authorize(Roles = "Manager")]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await userProServ.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
