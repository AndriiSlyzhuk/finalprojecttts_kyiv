﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService userServ;

        public UserController(IUserService userServ)
        {
            this.userServ = userServ;
        }

        // GET: api/<UserController>
        [Authorize(Roles = "Manager")]
        [HttpGet]
        public async Task<IEnumerable<UserModel>> Get()
        {
            try
            {
                return await userServ.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<UserController>/5
        [Authorize]
        [HttpGet("{id}")]
        public async Task<UserModel> Get(int id)
        {        
            
            try
            {
                //TODO: if user change role but using previous token, he will have access
                if (User.Identity.Name != id.ToString() && !User.IsInRole("Manager"))
                    throw new ApplicationException("Access is dinied");
                return await userServ.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<UserController>
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task Post([FromBody] UserNewModel model)
        {
            try
            {
                await userServ.AddAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<UserController>/5
        [Authorize(Roles = "Manager")]
        [HttpPut("update")]
        public async Task Put([FromBody] UserModel model)
        {
            try
            {
                await userServ.UpdateAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<UserController>/5
        [Authorize(Roles = "Manager")]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await userServ.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        
        [Authorize(Roles = "Manager")]
        [HttpGet("byRole/{role}")]
        public async Task<IEnumerable<UserModel>> GetAllByRole(string role)
        {
            try
            {
                return await userServ.GetAllByRole(role);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
